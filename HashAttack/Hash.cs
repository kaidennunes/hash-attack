﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashAttack
{
	public class Hash
	{
		public string HashedMessage { get; set; }
		public string OriginalMessage { get; set; }
	}
}
