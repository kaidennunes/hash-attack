﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Collections;

namespace HashAttack
{
	public static class SHA_1
	{
		private static readonly Encoding DEFAULT_ENCODING = Encoding.ASCII;


		public static string CalculateSHA1(string text, int numberOfBits)
		{
			return CalculateSHA1(text, numberOfBits, DEFAULT_ENCODING);
		}

		public static string CalculateSHA1(string text, int numberOfBits, Encoding encoding)
		{
			// Convert the input string to a byte array
			byte[] buffer = encoding.GetBytes(text);

			byte[] hash = null;
			using (SHA1CryptoServiceProvider transformSHA1 = new SHA1CryptoServiceProvider())
			{
				hash = transformSHA1.ComputeHash(buffer);
			}


			var numberOfCharacters = numberOfBits / 4;
			var stringHash = BitConverter.ToString(hash).Replace("-", "").Truncate(numberOfCharacters);

			return stringHash;
		}
	}
}
