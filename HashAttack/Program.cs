﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HashAttack
{
	class Program
	{
		private static StringBuilder CsvData = new StringBuilder();

		private static readonly string CSV_PATH = "C:\\Users\\Kaiden\\source\\repos\\HashAttack\\HashAttack\\CSV\\HashAttack.csv";

		private static readonly int NUMBER_OF_TESTS = 50;
		static void Main(string[] args)
		{
			CsvData.Append("Pre-image Attack");
			CsvData.Append(", ");
			CsvData.Append("\n");
			for (int i = 0; i < NUMBER_OF_TESTS; i++)
			{
				RunAllPreImageTests();
			}

			CsvData.Append("Collision Attack");
			CsvData.Append(", ");
			CsvData.Append("\n");
			for (int i = 0; i < NUMBER_OF_TESTS; i++)
			{
				RunAllCollisionTests();
			}
			// Print to the CSV file
			File.WriteAllText(CSV_PATH, CsvData.ToString());

			Console.ReadKey();
		}

		public static void RunAllPreImageTests()
		{
			Console.WriteLine("Running pre-image attack");
			RunPreImageAttack(8);
			RunPreImageAttack(12);
			RunPreImageAttack(16);
			RunPreImageAttack(20);
		}

		public static void RunAllCollisionTests()
		{
			Console.WriteLine("Running collision attack");
			RunCollisionAttack(8);
			RunCollisionAttack(16);
			RunCollisionAttack(20);
			RunCollisionAttack(24);
		}

		static void RunPreImageAttack(int numberOfBits)
		{
			var randomBeginningValue = GetRandomString();
			Hash beginningHash = new Hash
			{
				HashedMessage = SHA_1.CalculateSHA1(randomBeginningValue, numberOfBits),
				OriginalMessage = randomBeginningValue
			};

			long attempts = 0;
			bool duplicateHash = false;
			while (!duplicateHash)
			{
				var randomValue = GetRandomString();
				Hash hash = new Hash
				{
					HashedMessage = SHA_1.CalculateSHA1(randomValue, numberOfBits),
					OriginalMessage = randomValue
				};

				duplicateHash = CompareHashes(beginningHash.HashedMessage, hash.HashedMessage);
				attempts++;
			}

			var expectedAttempts = Math.Pow(2, numberOfBits);
			Console.WriteLine("Found duplicate after " + attempts + " attempts. Expected " + expectedAttempts + " tries");

			CsvData.Append(attempts);
			CsvData.Append(", ");
			CsvData.Append(expectedAttempts);
			CsvData.Append(", ");
			CsvData.Append("\n");
		}

		static void RunCollisionAttack(int numberOfBits)
		{
			List<Hash> calculatedHashes = new List<Hash>();

			bool duplicateHash = false;
			while (!duplicateHash)
			{
				var randomValue = GetRandomString();

				Hash hash = new Hash
				{
					HashedMessage = SHA_1.CalculateSHA1(randomValue, numberOfBits),
					OriginalMessage = randomValue
				};

				duplicateHash = calculatedHashes.Any(randomHash => CompareHashes(randomHash.HashedMessage, hash.HashedMessage));

				calculatedHashes.Add(hash);
			}
			var expectedAttempts = Math.Pow(2, numberOfBits / 2);
			Console.WriteLine("Found duplicate after " + calculatedHashes.Count + " attempts. Expected " + expectedAttempts + " tries");

			CsvData.Append(calculatedHashes.Count);
			CsvData.Append(", ");
			CsvData.Append(expectedAttempts);
			CsvData.Append(", ");
			CsvData.Append("\n");
		}

		static string GetRandomString()
		{
			using (RandomNumberGenerator randomGenerator = new RNGCryptoServiceProvider())
			{
				byte[] tokenData = new byte[32];
				randomGenerator.GetBytes(tokenData);
				return Convert.ToBase64String(tokenData);
			}
		}

		static bool CompareHashes(string hash1, string hash2)
		{
			return string.Equals(hash1, hash2);
		}
	}
}
